package br.com.usp.each.estacionaki.desktop.controller;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.JOptionPane;

import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.StatusVaga;
import br.com.usp.each.estacioaki.domain.Tamanho;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacionaki.desktop.fachada.EstacionamentoService;

public class AtualizarController implements Initializable {

	EstacionamentoService service = new EstacionamentoService();
	@FXML
	private TextField txtNome;
	
	@FXML
	private TextField txtCNPJ;
	
	@FXML
	private TextField txtIE;
	
	@FXML
	private TextField txtLogin;
	
	@FXML
	private PasswordField txtSenha;
	
	
	@FXML
	private TextField txtLocalidade;
	
	
	@FXML
	private TextField txtQtdeVagas;
	
	@FXML
	private TextField txt1Hora;
	
	@FXML
	private TextField txtDemaisHoras;
	
	private Estacionamento estacionamento;
	public void initialize(URL arg0, ResourceBundle arg1) {
		estacionamento = PrincipalController.estacionamentoCorrente;
		
		txtNome.setText(estacionamento.getNome());
		txtCNPJ.setText(estacionamento.getCNPJ());
		txtIE.setText(estacionamento.getIE());
		txtLogin.setText(estacionamento.getLogin());
		txtSenha.setText("");
		txtLocalidade.setText(estacionamento.getCoordenadas());
		txtQtdeVagas.setText(String.valueOf(estacionamento.getVagas().size()));
		txt1Hora.setText(new DecimalFormat("0.00").format(estacionamento.getValor1hora()).replace(",","."));
		txtDemaisHoras.setText(new DecimalFormat("0.00").format(estacionamento.getValorDemaisHoras()).replace(",","."));
	
		
		
	}
	

	public void btnCancelar(Event e){
		closeWindowsWithSomeEvent(e);
	}

	public void btnSalvar(Event e){
		if(txtSenha.getText().equals("") || txtSenha.getText().toString().length()<1){
			estacionamento.setSenha(null);
		}
		else{
			estacionamento.setSenha(txtSenha.getText());
		}
		
		estacionamento.setNome(txtNome.getText());
		estacionamento.setCNPJ(txtCNPJ.getText());
		estacionamento.setIE(txtIE.getText());
		estacionamento.setLogin(txtLogin.getText());
		estacionamento.setCoordenadas(txtLocalidade.getText());
		try {
			estacionamento.setValor1hora(Double.valueOf(txt1Hora.getText().replace(",",".")));
			estacionamento.setValorDemaisHoras(Double.valueOf(txtDemaisHoras.getText().replace(",",".")));
			
			int numeroDeVagas =Integer.parseInt(txtQtdeVagas.getText());
			if(numeroDeVagas != estacionamento.getVagas().size()){
				if(numeroDeVagas>estacionamento.getVagas().size()){
					int numeroVagasEst = estacionamento.getVagas().size();
					for(int i=0; i < numeroDeVagas-numeroVagasEst ; i++ ){
						Vaga v = new Vaga();
						v.setCoordenadas(estacionamento.getCoordenadas());
						v.setStatus(0);
						v.setStatusVaga(StatusVaga.DISPONIVEL);
						v.setTamanho(Tamanho.SEDAN);
						estacionamento.getVagas().add(v);
					}
				
				
				}
				else{
					int contador = 1;
					List<Vaga> vagas = new ArrayList<Vaga>();
					for(Vaga v : estacionamento.getVagas()){
						vagas.add(v);
						if((contador++) == numeroDeVagas){
							break;
						}
						
					}
					
					estacionamento.setVagas(vagas);
				}
			}
			if(estacionamento.getCNPJ().length()>1 &&
					estacionamento.getCoordenadas().length()>1 &&
					estacionamento.getIE().length()>1 &&
					estacionamento.getLogin().length()>1 &&
					estacionamento.getValor1hora()>1 &&
					estacionamento.getValorDemaisHoras()>1 &&
					estacionamento.getVagas().size()>0
					){
			estacionamento = service.atualizar(estacionamento);
				JOptionPane.showMessageDialog(null, "Cadastro atualizado com sucesso");
				PrincipalController.estacionamentoCorrente = estacionamento;
				closeWindowsWithSomeEvent(e);
			}
			else{
				JOptionPane.showMessageDialog(null, "Todos os campos devem ser preenchidos");
			}
		} catch (Exception e2) {
			JOptionPane.showMessageDialog(null, "Os campos de valores devem apenas conter números");
		}
		
		
		
	}

	private void closeWindowsWithSomeEvent(Event e) {
		Node  source = (Node) e.getSource();
		Stage stage  = (Stage) source.getScene().getWindow();
		stage.close();
	}
}
