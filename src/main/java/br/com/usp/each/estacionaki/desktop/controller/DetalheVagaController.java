package br.com.usp.each.estacionaki.desktop.controller;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import br.com.usp.each.estacioaki.domain.Cliente;
import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacionaki.desktop.domain.adapter.VagaAdapter;
import br.com.usp.each.estacionaki.desktop.fachada.EstacionamentoService;

public class DetalheVagaController  implements Initializable{

	private  VagaAdapter vagaAdapter;
	private  Estacionamento estacionamento;
	
	@FXML
	private Label lblVaga;
	
	@FXML
	private Label lblCliente;
	
	@FXML
	private Label lblVeiculo;
	
	@FXML
	private Label lblPlaca;
	
	@FXML
	private Label lblHorarioInicial;
	
	
	@FXML
	private Label lblValorParcial;

	@FXML
	private Button btnOk;

	
	public void initialize(URL arg0, ResourceBundle arg1) {
		vagaAdapter = PrincipalController.vagaAdapter;
		estacionamento = PrincipalController.estacionamentoCorrente;
		lblVaga.setText("Vaga "+vagaAdapter.getIdentificacaoVaga());
		if(vagaAdapter.getAluguel().getVeiculo()==null){
			lblCliente.setText("Cliente: Avulso");
			lblVeiculo.setText("Veiculo: Avulso");
			lblPlaca.setText("Placa: Avulso");
			
			
			
		}
		else{
			Cliente c = new EstacionamentoService().getClientePorVeiculo(vagaAdapter.getAluguel().getVeiculo());
			if(c !=null){
				lblCliente.setText("Cliente: "+c.getNome());
				lblVeiculo.setText("Veiculo: "+vagaAdapter.getAluguel().getVeiculo().getMarca().getMarca()+" - "+vagaAdapter.getAluguel().getVeiculo().getModelo().getModelo()+" - "+vagaAdapter.getAluguel().getVeiculo().getCor());
				lblPlaca.setText("Placa :"+vagaAdapter.getAluguel().getVeiculo().getPlaca());
			}
		}
		lblHorarioInicial.setText("Horario Inicial:"+new SimpleDateFormat("hh:mm:ss").format(vagaAdapter.getAluguel().getDataHoraInicial().getTime()));
		
		
		long mil =  Calendar.getInstance().getTimeInMillis() -vagaAdapter.getAluguel().getDataHoraInicial().getTimeInMillis();
		long segundos = mil/1000;
		long minutos = segundos/60;
		minutos-=60;
		double valorParcial = estacionamento.getValor1hora();
		while(minutos>0){
			valorParcial+= estacionamento.getValorDemaisHoras();
			minutos -=60;
		}
		lblValorParcial.setText("Valor Parcial :R$"+new DecimalFormat("0.00").format(valorParcial).replace(",","."));
		
	}
	
	public void btnfechar(ActionEvent e){
		closeWindowsWithSomeEvent(e);	
	}
	
	private void closeWindowsWithSomeEvent(ActionEvent e) {
		Node  source = (Node) e.getSource();
		Stage stage  = (Stage) source.getScene().getWindow();
		stage.close();
	}
}
