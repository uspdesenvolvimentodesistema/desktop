package br.com.usp.each.estacionaki.desktop.domain.adapter;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.StatusVaga;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacionaki.desktop.fachada.EstacionamentoService;

public class VagaAdapter extends Vaga{
	
	private Long identificacaoVaga;
	private String descricao;
	private Aluguel aluguel;
	private EstacionamentoService service = new EstacionamentoService();
	public Long getIdentificacaoVaga() {
		return identificacaoVaga;
	}
	
	public void setIdentificacaoVaga(Long identificacaoVaga) {
		this.identificacaoVaga = identificacaoVaga;
	}
	
	public VagaAdapter(Vaga vaga, Long identificacaoVaga,Aluguel aluguel){
		this.setIdentificacaoVaga(identificacaoVaga);
		this.setCoordenadas(vaga.getCoordenadas());
		this.setId(vaga.getId());
		this.setStatus(vaga.getStatus());
		this.setStatusVaga(vaga.getStatusVaga());
		this.setTamanho(vaga.getTamanho());
		this.aluguel = aluguel;
		if(vaga.getStatusVaga().equals(StatusVaga.RESERVADO)){
			if(aluguel==null ){
				setAluguel(service.AluguelVaga(vaga));
			}
			
			if(aluguel!=null && aluguel.getVeiculo()!=null)
				descricao = "Placa: "+aluguel.getVeiculo().getPlaca()+" \tVeiculo: "+aluguel.getVeiculo().getMarca().getMarca()+" - "+aluguel.getVeiculo().getModelo().getModelo()+" - "+aluguel.getVeiculo().getCor();
			else
				descricao="";
		}
		else if(vaga.getStatusVaga().equals(StatusVaga.OCUPADO)){
			if(aluguel==null || aluguel.getVeiculo()==null){
				descricao = "Cliente Avulso";
			}
			else{
				descricao = "Placa: "+aluguel.getVeiculo().getPlaca()+" \tVeiculo: "+aluguel.getVeiculo().getMarca().getMarca()+" - "+aluguel.getVeiculo().getModelo().getModelo()+" - "+aluguel.getVeiculo().getCor();
			}
		}
		else{
			descricao ="";
		}
	}
	
	public String getDescricao() {
		return descricao.toUpperCase() ;
	}
	
	
	public void setAluguel(Aluguel aluguel) {
		this.aluguel = aluguel;
	}
	
	public Aluguel getAluguel() {
		return aluguel;
	}
	
	

}
