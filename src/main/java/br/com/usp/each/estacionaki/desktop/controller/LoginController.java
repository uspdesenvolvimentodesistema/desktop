package br.com.usp.each.estacionaki.desktop.controller;

import java.io.IOException;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import javax.swing.JOptionPane;

import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacionaki.desktop.fachada.EstacionamentoService;

public class LoginController {
	private Parent mainPage;
	private FXMLLoader fxmlLoader = new FXMLLoader();
	
	@FXML
	Button btnLogar;
	
	@FXML
	Button btnCancelar;
	
	@FXML
	PasswordField txtSenha;
	
	@FXML
	TextField txtLogin;
	private static Estacionamento estacionamento;
	
	
	public void FinalizarSistema() throws IOException{
//		if(JOptionPane.showConfirmDialog(null, "Tem certeza que deseja finalizar o sistema?","Finalizar Sistema ESCA",JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE)==JOptionPane.YES_OPTION){
//			System.exit(0);
//		}
		Stage cadastroStage = setStage("/fxml/cadastro.fxml");
		cadastroStage.showAndWait();
		
	}
	public Stage setStage(String path) throws IOException {
		Stage stagePesquisar = new Stage();
		stagePesquisar.centerOnScreen();
		stagePesquisar.initModality(Modality.APPLICATION_MODAL);
		stagePesquisar.initStyle(StageStyle.TRANSPARENT);
		AnchorPane productPage;
		try {

			productPage = (AnchorPane) new FXMLLoader().load(getClass().getResourceAsStream(path));
			Scene scene = new Scene(productPage);
			scene.setFill(Color.WHITE);
			stagePesquisar.setScene(scene);
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return stagePesquisar;
	}
	
	@FXML
	public void EntrarNoSistema(Event e) throws IOException{
		if(txtLogin.getText().length()==0 || txtSenha.getText().length()==0){
			JOptionPane.showMessageDialog(null, "Os campos Login e senha devem ser preenchidos!!!");
		
		}
		else{
			
			Estacionamento estacionamento = new Estacionamento();
			estacionamento.setLogin(txtLogin.getText());
			estacionamento.setSenha(txtSenha.getText());
			EstacionamentoService service = new EstacionamentoService();
			estacionamento = service.login(estacionamento);
			if(estacionamento!=null){
				LoginController.estacionamento = estacionamento;
				
				
				Stage stage= (Stage) ((Node) e.getSource()).getScene().getWindow();
				mainPage = (Parent) fxmlLoader.load(getClass().getResourceAsStream("/fxml/principal.fxml"));
				stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			         public void handle(final WindowEvent windowEvent) {
			        	 JOptionPane.showMessageDialog(null, "O sistema esta sendo finalizado");
			        	   System.exit(0);
			         }
				 	});
				Scene scene = new Scene(mainPage);
				stage.setScene(scene);
				
				stage.setTitle("EstacionaeAki");
				//stage.setFullScreen(true);
				
				stage.setResizable(false); 
				stage.setHeight(725); 
				stage.setWidth(900); 
				stage.centerOnScreen(); 
				JOptionPane.showMessageDialog(null, "Login efetuado com sucesso para usuario "+estacionamento.getNome());
			}
			
			else{
				JOptionPane.showMessageDialog(null, "Login ou senha não conferem");
			}
			
	
		}
	}
	
	public static Estacionamento getEstacionamento() {
		return LoginController.estacionamento;
	}
	
}
