package br.com.usp.each.estacionaki.desktop.fachada;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Cliente;
import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacioaki.domain.Veiculo;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class EstacionamentoService {
	private static String URL ="http://192.168.1.43:8080/servicos_estacionamento/estacionamento/";
	private static String URL_CLIENTE ="http://192.168.1.43:8080/servicos_cliente/cliente/";
	
	
	public Estacionamento login(Estacionamento estacionamento) {
		Client client = Client.create();
		// http://localhost:8080/WebService/cliente/erico
		WebResource webResource = client.resource(URL + "login");

		Gson gson = new Gson();
		String input = gson.toJson(estacionamento);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		System.out.println("Output from Server .... \n");
		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Estacionamento result = gson.fromJson(output, Estacionamento.class);
			System.out.println(result.getNome() + " " + result.getId());
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Estacionamento find(Estacionamento estacionamento) {
		Client client = Client.create();
		// http://localhost:8080/WebService/cliente/erico
		WebResource webResource = client.resource(URL + "find");

		Gson gson = new Gson();
		String input = gson.toJson(estacionamento);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		System.out.println("Output from Server .... \n");
		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Estacionamento result = gson.fromJson(output, Estacionamento.class);
			System.out.println(result.getNome() + " " + result.getId());
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Aluguel OcuparVaga(Vaga vaga) {
		Client client = Client.create();
		
		WebResource webResource = client.resource(URL + "ocuparVaga");

		Gson gson = new Gson();
		String input = gson.toJson(vaga);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Aluguel result = gson.fromJson(output, Aluguel.class);
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Aluguel LiberarVaga(Vaga vaga) {
		Client client = Client.create();
		
		WebResource webResource = client.resource(URL + "liberarVaga");

		Gson gson = new Gson();
		String input = gson.toJson(vaga);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Aluguel result = gson.fromJson(output, Aluguel.class);
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Aluguel AluguelVaga(Vaga vaga) {
		Client client = Client.create();
		
		WebResource webResource = client.resource(URL + "aluguelVaga");

		Gson gson = new Gson();
		String input = gson.toJson(vaga);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Aluguel result = gson.fromJson(output, Aluguel.class);
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Aluguel ReservaOcupadaVaga(Aluguel aluguel) {
		Client client = Client.create();
		
		WebResource webResource = client.resource(URL + "reservaOcupada");

		Gson gson = new Gson();
		String input = gson.toJson(aluguel);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Aluguel result = gson.fromJson(output, Aluguel.class);
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Cliente  getClientePorVeiculo(Veiculo v) {
		Client client = Client.create();
		
		WebResource webResource = client.resource(URL_CLIENTE + "clientePorVeiculo");

		Gson gson = new Gson();
		String input = gson.toJson(v);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Cliente result = gson.fromJson(output, Cliente.class);
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Estacionamento cadastrar(Estacionamento estacionamento) {
		Client client = Client.create();
		// http://localhost:8080/WebService/cliente/erico
		WebResource webResource = client.resource(URL + "cadastro");

		Gson gson = new Gson();
		String input = gson.toJson(estacionamento);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		System.out.println("Output from Server .... \n");
		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Estacionamento result = gson.fromJson(output, Estacionamento.class);
			System.out.println(result.getNome() + " " + result.getId());
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	public Estacionamento atualizar(Estacionamento estacionamento) {
		Client client = Client.create();
		// http://localhost:8080/WebService/cliente/erico
		WebResource webResource = client.resource(URL + "atualizar");

		Gson gson = new Gson();
		String input = gson.toJson(estacionamento);
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, input);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		System.out.println("Output from Server .... \n");
		String output = response.getEntity(String.class);
		try {
			System.out.println(output);

			Estacionamento result = gson.fromJson(output, Estacionamento.class);
			System.out.println(result.getNome() + " " + result.getId());
			return result;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
	
	
}
