package br.com.usp.each.estacionaki.desktop;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.swing.JOptionPane;

/**
 * Hello world!
 *
 */
public class App extends Application{
	
    	private FXMLLoader fxmlLoader = new FXMLLoader();
    	@Override
    	public void start(final Stage stage) {
    		
    	   Parent parent;
    		try {
//    			InputStream file = new BufferedInputStream(
//    		            new FileInputStream("C:\\esca\\fxml\\FormLogin.fxml"));
    			parent = (Parent) fxmlLoader.load(getClass().getResourceAsStream("/fxml/login.fxml"));//FXMLLoader.load(getClass().getResource("/br/com/e7code/esca/view/login/FormLogin.fxml"));
    			
    			 Scene scene = new Scene(parent);  
    		        stage.setScene(scene);
    		        stage.setTitle("EstacioneAki");
    		        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
    			         public void handle(final WindowEvent windowEvent) {
    			        	 JOptionPane.showMessageDialog(null, "O sistema esta sendo finalizado");
    			        	   System.exit(0);
    			          
    			        	   
    			         }
    				 	});
    		        stage.show();
    		      
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}  
    	       
    	}
    	
    	public static void main(String[] args) {
    		launch(args);
    	}
    
}
