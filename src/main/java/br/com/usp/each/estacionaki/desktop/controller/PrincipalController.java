package br.com.usp.each.estacionaki.desktop.controller;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.swing.JOptionPane;

import br.com.usp.each.estacioaki.domain.Aluguel;
import br.com.usp.each.estacioaki.domain.Estacionamento;
import br.com.usp.each.estacioaki.domain.StatusVaga;
import br.com.usp.each.estacioaki.domain.Vaga;
import br.com.usp.each.estacionaki.desktop.arduino.Arduino;
import br.com.usp.each.estacionaki.desktop.domain.adapter.VagaAdapter;
import br.com.usp.each.estacionaki.desktop.fachada.EstacionamentoService;

public class PrincipalController implements Initializable{
	private static String STATUS_ARDUINO = "3";
	
	EstacionamentoService service = new EstacionamentoService();
	static  Estacionamento estacionamentoCorrente = null;
	@FXML
	Label lblEstacionamento;
	
	@FXML
	private TableView<VagaAdapter> tableVagas;
	
	@FXML
	private TableColumn<VagaAdapter, String> id;
	
	@FXML
	private TableColumn<VagaAdapter, String> status;
	
	@FXML
	private TableColumn<VagaAdapter, String> descricao;
	
	
	
	@FXML
	private TextField txtCodigoBarras;
//
//	@FXML
//	private Button btnConfiguracoes;
	
	
	private Arduino arduino;
	static int CONFIRMACAO = 1;
	
	private int livreParaOcupado[]; 
	private int ocupadoParaLivre[]; 
	private int reservaParaOcupado[];
	private Aluguel[] alugueis;
	private boolean pause = false;
	public void initialize(URL arg0, ResourceBundle arg1) {
		txtCodigoBarras.setOpacity(0);
		txtCodigoBarras.requestFocus();
		try {
			inicializarArduino();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		id.setText("Vaga");
		id.setCellValueFactory(new PropertyValueFactory<VagaAdapter, String>("identificacaoVaga"));
		status.setCellValueFactory(new PropertyValueFactory<VagaAdapter, String>("statusVaga"));
		descricao.setCellValueFactory(new PropertyValueFactory<VagaAdapter, String>("descricao"));
		
		estacionamentoCorrente = LoginController.getEstacionamento();
		lblEstacionamento.setText(estacionamentoCorrente.getNome());
		if(estacionamentoCorrente !=null){
			
			livreParaOcupado = new int[estacionamentoCorrente.getVagas().size()];
			ocupadoParaLivre = new int[estacionamentoCorrente.getVagas().size()];
			reservaParaOcupado = new int[estacionamentoCorrente.getVagas().size()];
			alugueis = new Aluguel[estacionamentoCorrente.getVagas().size()];
			atualizarStatusVagas();
		}
		
		new Thread(new Runnable() {
		     public void run() {
		      while(true){
		    	  try {
		    		  Thread.sleep(1234l);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		            Platform.runLater(new Runnable() {
		                public void run() {
		                    try {
		                    	if(!pause){
		                    		sincronicarComArduino();
									txtCodigoBarras.requestFocus();
		                    	}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		                }
		            });
		        }
		     
		    }
		}).start();
		
	}
	public Stage setStage(String path) throws IOException {
		Stage stagePesquisar = new Stage();
		stagePesquisar.centerOnScreen();
		stagePesquisar.initModality(Modality.APPLICATION_MODAL);
		stagePesquisar.initStyle(StageStyle.TRANSPARENT);
		AnchorPane productPage;
		try {

			productPage = (AnchorPane) new FXMLLoader().load(getClass().getResourceAsStream(path));
			Scene scene = new Scene(productPage);
			scene.setFill(Color.WHITE);
			stagePesquisar.setScene(scene);
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return stagePesquisar;
	}

	private void inicializarArduino() {
		arduino = new Arduino("COM8", 9600, 0);
		arduino.ObterIdDaPorta();
		arduino.AbrirPorta();
		arduino.HabilitarLeitura();
		arduino.LerDados();
		arduino.HabilitarEscrita();
	}
	
	private void encerrarArduino() {
		arduino.FecharCom();
	}
	

	private void atualizarStatusVagas() {
		tableVagas.setItems(null);
		ObservableList<VagaAdapter> items = FXCollections.observableArrayList();
		Long contador = 1l;
		int contadorInt = 0;
		for (Vaga itemP : estacionamentoCorrente.getVagas()) {
			if (itemP != null){
				VagaAdapter v =new VagaAdapter(itemP, contador++,alugueis[contadorInt++]);
				items.add(v);
			}
		}
		tableVagas.setItems(items);
	}
	
	private Long getIdentificadorVaga(Vaga vaga) {
		Long contador = 1l;
		for (Vaga cadaVaga : estacionamentoCorrente.getVagas()) {
			if (cadaVaga != null && cadaVaga.getId().equals(vaga.getId())){
				return contador;
			}
			else{
				contador++;
			}
		}
		return contador;
	}
	
	private Long getIdentificadorVaga(long vagaId) {
		Long contador = 1l;
		for (Vaga cadaVaga : estacionamentoCorrente.getVagas()) {
			if (cadaVaga != null && cadaVaga.getId().equals(vagaId)){
				return contador;
			}
			else{
				contador++;
			}
		}
		return contador;
	}
	
	private void sincronicarComArduino() throws InterruptedException, NumberFormatException{
		arduino.EnviarUmaString(STATUS_ARDUINO);
		if(arduino.getRetorno()!=null){
		sicronizarReservas();
		String retornoArduino = arduino.getRetorno().toString();
		String retornoArduinoVagas[] = retornoArduino.replace("\n", " ").split(" ");
		String StringVagas[] = new String[Math.max((int) Math.ceil((retornoArduinoVagas.length/2d)),estacionamentoCorrente.getVagas().size())];
		for(int i=0 ; i<StringVagas.length;i++){
			StringVagas[i]="0";
		}
		for(int i=0,j=0 ; i<retornoArduinoVagas.length;i++){
			if(!(retornoArduinoVagas[i].equals(""))){
				StringVagas[j++] = retornoArduinoVagas[i];
			}
		}
		for(int i=0;i<estacionamentoCorrente.getVagas().size();i++){
			if(!(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(mapStatusVaga.get(Integer.parseInt(StringVagas[i]))))){
				sincronizarTabelaComArduino();
				if(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(StatusVaga.DISPONIVEL)
						&& mapStatusVaga.get(Integer.parseInt(StringVagas[i])).equals(StatusVaga.OCUPADO)){
					livreParaOcupado[i]++;
					if(livreParaOcupado[i]>CONFIRMACAO){
						alugueis[i] = service.OcuparVaga(estacionamentoCorrente.getVagas().get(i));
						trocarVaga(estacionamentoCorrente.getVagas().get(i),alugueis[i].getVaga());
					}
				}
				else if(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(StatusVaga.OCUPADO)
							&& mapStatusVaga.get(Integer.parseInt(StringVagas[i])).equals(StatusVaga.DISPONIVEL)){
					ocupadoParaLivre[i]++;
					
					if(ocupadoParaLivre[i]>CONFIRMACAO){
						alugueis[i]=service.LiberarVaga(estacionamentoCorrente.getVagas().get(i));
						trocarVaga(estacionamentoCorrente.getVagas().get(i), alugueis[i].getVaga());
						pause=true;
						JOptionPane.showMessageDialog(null, "Vaga "+(i+1)+"\n"
														+"Chegada: "+new SimpleDateFormat("hh:mm:ss").format(alugueis[i].getDataHoraInicial().getTime())+ "\n"
														+"Saida: "+new SimpleDateFormat("hh:mm:ss").format(Calendar.getInstance().getTime())+ "\n"
														+"Valor a ser pago: R$"+new DecimalFormat("0.00").format(valorAserPago(Calendar.getInstance(), alugueis[i]))); 
						pause=false;
					}
					
				}
				else if(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(StatusVaga.RESERVADO)
						&& mapStatusVaga.get(Integer.parseInt(StringVagas[i])).equals(StatusVaga.OCUPADO)){
					reservaParaOcupado[i]++;
					if(reservaParaOcupado[i]>CONFIRMACAO){
						alugueis[i] = service.ReservaOcupadaVaga(alugueis[i]);
						trocarVaga(estacionamentoCorrente.getVagas().get(i),alugueis[i].getVaga());
					}
				}
				else
					try {
						if(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(StatusVaga.DISPONIVEL) && 
								mapStatusVaga.get(Integer.parseInt(StringVagas[i])).equals(StatusVaga.RESERVADO)){
							new Thread();
							Thread.sleep(450l);
							arduino.EnviarUmaString(getIdentificadorVaga(estacionamentoCorrente.getVagas().get(i))+"0");
							Thread.sleep(50l);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			else{
				ocupadoParaLivre[i] = livreParaOcupado[i] = reservaParaOcupado[i] =0;
			}
			
		}
		atualizarStatusVagas();
		}
	}
	
	private static HashMap<Integer, StatusVaga> mapStatusVaga = new HashMap<Integer, StatusVaga>();
	static
	{
		mapStatusVaga.put(0, StatusVaga.DISPONIVEL);
		mapStatusVaga.put(1, StatusVaga.OCUPADO);
		mapStatusVaga.put(2, StatusVaga.RESERVADO);
	}
	
	private void trocarVaga(Vaga vaga1, Vaga vaga2){
		vaga1.setCoordenadas(vaga2.getCoordenadas());
		vaga1.setId(vaga2.getId());
		vaga1.setStatus(vaga2.getStatus());
		vaga1.setStatusVaga(vaga2.getStatusVaga());
		vaga1.setTamanho(vaga2.getTamanho());
		
		
	}
	
	private void sincronizarTabelaComArduino() throws InterruptedException{
		for(int i=0;i<estacionamentoCorrente.getVagas().size();i++){
			if(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(StatusVaga.RESERVADO)){
				new Thread();
				Thread.sleep(450l);
				arduino.EnviarUmaString(getIdentificadorVaga(estacionamentoCorrente.getVagas().get(i))+"1");
				
				Thread.sleep(50l);
			}
		}
	}
	
	private void sicronizarReservas(){
		Estacionamento e = new Estacionamento();
		e.setId(estacionamentoCorrente.getId());
		e = service.find(e);
		
		
		for(int i=0;i<e.getVagas().size();i++){
			
			
			//reserva
			if(e.getVagas().get(i).getStatusVaga().equals(StatusVaga.RESERVADO)){
				if(!(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(StatusVaga.RESERVADO))){
					estacionamentoCorrente.getVagas().get(i).setStatusVaga(StatusVaga.RESERVADO);
					arduino.EnviarUmaString(getIdentificadorVaga(estacionamentoCorrente.getVagas().get(i))+"1");
				}
			}
			//cancelamento da Reserva
			else if(e.getVagas().get(i).getStatusVaga().equals(StatusVaga.DISPONIVEL)){
				if(estacionamentoCorrente.getVagas().get(i).getStatusVaga().equals(StatusVaga.RESERVADO)){
					estacionamentoCorrente.getVagas().get(i).setStatusVaga(StatusVaga.DISPONIVEL);
					arduino.EnviarUmaString(getIdentificadorVaga(estacionamentoCorrente.getVagas().get(i))+"0");
				}
			}
			
			alugueis[i] = service.AluguelVaga(estacionamentoCorrente.getVagas().get(i));
		}
	}
	public void lerCodigoDeBarras(KeyEvent e) throws InterruptedException {
		if(e.getCode().equals(KeyCode.TAB) ||
				e.getCode().equals(KeyCode.ENTER)){
			
			if(txtCodigoBarras.getText().toString().length()>0){
				String ret = txtCodigoBarras.getText();
				String dados[] =ret.replace(".", " ").split(" ");
				
				if(dados.length==3){
					long vagaId =  Long.parseLong(dados[1]);
					Thread.sleep(500l);
					arduino.EnviarUmaString(getIdentificadorVaga(vagaId)+"5");
					Thread.sleep(500l);
					
					
				}
			}
			txtCodigoBarras.setText("");
		}
	}
	
	public void selecionarItem(MouseEvent e) throws IOException {
		if (e.getButton().equals(MouseButton.PRIMARY)) {
			if (e.getClickCount() == 2) {
				if (!(tableVagas.getSelectionModel().getSelectedIndex() < -1)) {
					if(alugueis[tableVagas.getSelectionModel().getSelectedIndex()]!=null){
						
						vagaAdapter = 
								new VagaAdapter(
										estacionamentoCorrente.getVagas().get(tableVagas.getSelectionModel().getSelectedIndex()), 
										tableVagas.getSelectionModel().getSelectedIndex()+1l, 
										alugueis[tableVagas.getSelectionModel().getSelectedIndex()]);
						Stage stageDetalhe = setStage("/fxml/detalheVaga.fxml");
						stageDetalhe.showAndWait();
					}
					else{
						
						JOptionPane.showMessageDialog(null, "A vaga está disponivel");
					}
				}
			}
		
		}
	}
	
	public Double valorAserPago(Calendar timeFinal, Aluguel aluguel){
		long mil =  timeFinal.getTimeInMillis() - aluguel.getDataHoraInicial().getTimeInMillis();
		long segundos = mil/1000;
		long minutos = segundos/60;
		minutos-=60;
		double valorParcial = estacionamentoCorrente.getValor1hora();
		while(minutos>0){
			valorParcial+= estacionamentoCorrente.getValorDemaisHoras();
			minutos -=60;
		}
		
		return valorParcial;
	}
	
	static VagaAdapter vagaAdapter;
	
	@FXML
	public void atualizarCadastro(Event e) throws IOException{
		Stage stageDetalhe =  setStage("/fxml/atualizar.fxml");
		pause=true;
		stageDetalhe.showAndWait();
		pause=false;
		if(estacionamentoCorrente !=null){
			
			livreParaOcupado = new int[estacionamentoCorrente.getVagas().size()];
			ocupadoParaLivre = new int[estacionamentoCorrente.getVagas().size()];
			reservaParaOcupado = new int[estacionamentoCorrente.getVagas().size()];
			alugueis = new Aluguel[estacionamentoCorrente.getVagas().size()];
			lblEstacionamento.setText(estacionamentoCorrente.getNome());
		}
	}
}
